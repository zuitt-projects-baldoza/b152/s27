	// console.log(req.url); 
	// // request URL endpoint
	// console.log(req.method); 
	// // request method


let http = require("http");

//Mock Data
let users = [
	
	{
		username: "moonknight1999",
		email: "moonGod@gmail.com",
		password: "moonKnightStrong"
	},
	{
		username: "kitkatMachine",
		email: "notSponsored@gmail.com",
		password: "kitkatForever"
	}

];

let courses = [
	{
		name: "Science 101",
		price: 2500,
		isActive: true
	},
	{
		name: "English 101",
		price: 2500,
		isActive: true
	}
];


http.createServer((req,res)=>{

	if (req.url === "/" && req.method === "GET"){
		res.writeHead(200,{'Content-Type' : 'text/plain'});
		res.end("Hello World! This route checks for GET Method.")

	} else if (req.url === "/" && req.method === "POST"){
		res.writeHead(200,{'Content-Type' : 'text/plain'});
		res.end("Hello World! This route checks for POST Method.")
	} else if(req.url === "/" && req.method === 'PUT'){
		res.writeHead(200,{'Content-Type':'text/plain'})
		res.end("Hello World! This route checks for PUT Method")
	} else if(req.url === "/" && req.method === "DELETE"){
		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end("Hello World! This route checks for DELETE Method")

} else if (req.url === "/users" && req.method === "POST"){
let requestBody = "";
req.on('data', function(data){
	// console.log(data);
	requestBody += data;
})

req.on('end',function(){
	console.log(requestBody);
	requestBody = JSON.parse(requestBody);

//Simulate creating a document and adding into a collection
let newUser = {
	username: requestBody.username,
	email: requestBody.email,
	password: requestBody.password
}
	users.push(newUser);
	console.log(users);

	res.writeHead(200,{'Content-Type':'application/json'});
	res.end(JSON.stringify(users));
})

	} else if(req.url === "/courses" && req.method === "GET"){
		res.writeHead(200,{'Content-Type':'application/JSON'});
		res.end(JSON.stringify(courses));

// Mini-Activity
} else if(req.url === "/courses" && req.method === "POST"){
let requestBody = "";
req.on('data', function(data){
	// console.log(data);
	requestBody += data;
})

req.on('end',function(){
	console.log(requestBody);
	requestBody = JSON.parse(requestBody);

let newCourse = {
	name: requestBody.name,
	price: requestBody.price,
	isActive: requestBody.isActive
}
	courses.push(newCourse);
	console.log(courses);

	res.writeHead(200,{'Content-Type':'application/json'});
	res.end(JSON.stringify(courses));
})

	// Mini-Activity
	// } else if(req.url === "/users" && req.method === "GET"){
	// 	res.writeHead(200,{'Content-Type':'application/JSON'});
	// 	res.end(JSON.stringify(users));

	// } else if(req.url === "/users" && req.method === "POST"){
	// 	res.writeHead(200,{'Content-Type':'text/plain'});
	// 	res.end("Hello, User! You are creating a new user.")

	// Mini-Activity
	}

}).listen(4000);

console.log('Server is running on localhost:4000')